package com.latentview.classification.core

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

trait AppBuilder {

  val conf: org.apache.spark.SparkConf = new SparkConf().setMaster("local[*]").setAppName(getClass.getSimpleName).set("spark.cleaner.periodicGC.interval", "30").set("spark.scheduler.mode", "FAIR").set("spark.cleaner.referenceTracking", "true").set("spark.executor.memory", "18g").set("spark.num.executors", "29").set("spark.executor.cores", "5").set("spark.local.dir", "/tmp/temp").set("spark.driver.maxResultSize", "3g").set("spark.network.timeout", "10000000s").set("spark.sql.broadcastTimeout", "36000000").set("spark.executor.heartbeatInterval", "100s").set("spark.driver.memory", "10g").set("spark.driver.allowMultipleContexts", "true").set("spark.scheduler.mode", "FAIR").set("spark.shuffle.service.enabled", "true").set("spark.shuffle.service.port", "7337")

  val Spark: org.apache.spark.sql.SparkSession = SparkSession.builder.appName("bmw smart insight").config(conf).getOrCreate()

  Spark.sparkContext.setLocalProperty("spark.scheduler.pool", "production")

}

object AppBuilder extends AppBuilder {
  def apply(): AppBuilder = new AppBuilder {}
}
