#SBT Documentation URL:
https://www.scala-sbt.org/1.x/docs/sbt-by-example.html

#COMMANDS

sbt clean
sbt compile
sbt reload
sbt run
sbt assembly
sbt evicted