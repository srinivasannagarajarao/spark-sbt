val fatJarName = taskKey[String]("To Get Fat Jar Name Dynamically")

ThisBuild / scalaVersion := "2.12.7"
ThisBuild / organization := "com.latentview"
ThisBuild / version := "2.0.0"
val sparkVersion = "2.4.1"
val scope = "provided"

lazy val spark_run = taskKey[Unit]("Builds the assembly and ships it to the Spark Cluster")

lazy val hello = (project in file("."))
  .settings(
    name := "spark-sbt",
    assemblyMergeStrategy in assembly := {
      case PathList("org", "aopalliance", xs@_*) => MergeStrategy.last
      case PathList("javax", "inject", xs@_*) => MergeStrategy.last
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.last
      case PathList("javax", "activation", xs@_*) => MergeStrategy.last
      case PathList("org", "apache", xs@_*) => MergeStrategy.last
      case PathList("com", "google", xs@_*) => MergeStrategy.last
      case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.last
      case PathList("com", "codahale", xs@_*) => MergeStrategy.last
      case PathList("com", "yammer", xs@_*) => MergeStrategy.last
      case "about.html" => MergeStrategy.rename
      case "META-INF/ECLIPSEF.RSA" => MergeStrategy.last
      case "META-INF/mailcap" => MergeStrategy.last
      case "META-INF/mimetypes.default" => MergeStrategy.last
      case "plugin.properties" => MergeStrategy.last
      case "log4j.properties" => MergeStrategy.last
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },

    assemblyJarName in assembly := s"${name.value}-${version.value}.jar", //assembly and package jar name declaration differs

    spark_run := {
      ("spark_submit " + assembly.value)
      println(assembly.value)
    },
    libraryDependencies ++= Seq(
      "com.typesafe.play" %% "play-json" % "2.6.9",
      "com.eed3si9n" %% "gigahorse-okhttp" % "0.3.1",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "org.apache.spark" %% "spark-core" % sparkVersion % scope,
      "org.apache.spark" %% "spark-sql" % sparkVersion % scope,
      "org.apache.spark" %% "spark-mllib" % sparkVersion % scope,
      "org.apache.spark" %% "spark-streaming" % sparkVersion % scope,
      "org.apache.spark" %% "spark-hive" % sparkVersion % scope
    )
  )
