import subprocess
import os
import paramiko
from scp import SCPClient

# pip install scp
BASEDIR = os.path.dirname(os.getcwd())
server = "192.168.0.28"
port = "22"
user = "srinivasan"
password = "Welcome@123"

p = subprocess.run(['sbt', 'clean', 'assembly'], cwd=BASEDIR, stdout=subprocess.PIPE).stdout.decode('utf-8')
print(p)


def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client


def moveFilesToRemoteHost():
    ssh = createSSHClient(server, port, user, password)
    scp = SCPClient(ssh.get_transport())
    scp.put("/home/srinivasan/examples.desktop", "/home/srinivasan/examples.desktop1")
